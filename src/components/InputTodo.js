import React, { Component } from 'react'

export default class InputTodo extends Component {
  constructor(props) {
    super(props);
    this.state = { todos: [], time: '', activity: '' }
    this.timeChange = this.timeChange.bind(this);
    this.activityChange = this.activityChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  timeChange(e) {
    this.setState({ time: e.target.value});
  }
  activityChange(e) {
    this.setState({ activity: e.target.value});
  }
  handleSubmit(e) {
    e.preventDefault();
    if (this.state.time.length === 0 || this.state.activity === 0) {
      return;
    }

    const todo = {
      id: Date.now(),
      time: this.state.time,
      activity: this.state.activity
    };
    console.log(todo);
    this.setState( state => ({
      todos: state.todos.concat(todo),
      time: '',
      activity: '',
    }));
    console.log(this.state.todos);

  }
  render() {
    return (
      <div>
        <form className="form-inline" onSubmit={this.handleSubmit}>
          <label htmlFor="time-todo">Time : </label>
          <input
            id="time-todo"
            onChange={this.timeChange}
            value={this.state.time}
            className="form-control"
            placeholder="Waktu Aktivitas"
          />
          <label htmlFor="activity-todo">Activity : </label>
          <input
            size="50"
            id="activity-todo"
            onChange={this.activityChange}
            value={this.state.activity}
            className="form-control"
            placeholder="Uraian Rencana Aktivitas"
          />
          <button className="btn btn-info">
            Simpan Data
          </button>
        </form>
        <hr />
        <TodoList todos={this.state.todos} />
      </div>
    )
  }
}

class TodoList extends React.Component {
  render() {
    return (
      <ul>
        {
          this.props.todos.map(item => (
            <li key={item.id}>
              <button className="btn btn-outline-danger">Hapus</button>
              {item.time} : {item.activity}
            </li>
          ))}
      </ul>
    )
  }
}
