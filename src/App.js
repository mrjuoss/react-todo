import React, { Component } from 'react';
import Header from './components/Header';
import InputTodo from './components/InputTodo';
import './App.css';

class App extends Component {

  render() {
  return (
    <div>
      <Header />
      <InputTodo />
    </div>
  );
  }
}

export default App;
